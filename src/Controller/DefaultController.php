<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Form\MovieFormType;
use App\Service\MovieService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    /**
     * @Route("/", name="moviesPage")
     * @param MovieService $movieService
     */
    public function moviesPage(MovieService $movieService): Response
    {
        $movies = $movieService->getPopularMovies();
        return $this->render('Home/home.html.twig', [
            'movies' => $movies
        ]);
    }

    /**
     * @Route("/movie/detail/{id}", name="detailPage")
     */
    public function detail(MovieService $movieService, $id): Response
    {
        $movie = $movieService->getMovieById($id);
        $recommendations = $movieService->getRecommendations($id);
        $recommendations = array_slice($recommendations, 0, 10);
        return $this->render('Detail/detail.html.twig', [
            'movie' => $movie,
            'recommendations' => $recommendations
        ]);
    }

}